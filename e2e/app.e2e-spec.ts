import { ShrimadhavUKPage } from './app.po';

describe('shrimadhav-u-k App', () => {
  let page: ShrimadhavUKPage;

  beforeEach(() => {
    page = new ShrimadhavUKPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
